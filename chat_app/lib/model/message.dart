class Message {
  String message;
  int timeCreate;
  String idSender;
  bool itMe;

  Message({this.message, this.timeCreate, this.idSender});

  Message.fromJson(dynamic data) {
    message = data['message'];
    timeCreate = data['time_create'];
    idSender = data['id_sender'];
  }

  Map<String, dynamic> toJson() {
    final data = Map<String, dynamic>();
    data['message'] = message;
    data['time_create'] = timeCreate;
    data['id_sender'] = idSender;
    return data;
  }
}
