import 'package:flutter/material.dart';

class User {
  String name;
  String id;
  int timeCreate;

  User({
    @required this.name,
    @required this.id,
    this.timeCreate,
  });

  User.fromJson(Map<String, dynamic> data) {
    name = data['name'];
    id = data['id'];
    timeCreate = data['time_create'];
  }

  Map<String, dynamic> toJson() {
    final data = Map<String, dynamic>();
    data['name'] = name;
    data['id'] = id;
    data['time_create'] = timeCreate;
    return data;
  }
}
