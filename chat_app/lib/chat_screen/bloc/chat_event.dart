import 'package:chatapp/model/user.dart';

abstract class ChatEvent {}

class InitChatEvent extends ChatEvent {}

class GetDataEvent extends ChatEvent {
  final String idGroupChat;
  final String idCurrentUser;

  GetDataEvent({this.idGroupChat, this.idCurrentUser});
}

class SendMessageEvent extends ChatEvent {
  final String message;
  final String idSender;
  final String idGroup;

  SendMessageEvent({this.message, this.idSender, this.idGroup});
}
