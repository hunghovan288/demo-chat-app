import 'package:chatapp/model/message.dart';

abstract class ChatState {
  final List<Message> listMessage;

  ChatState({this.listMessage});
}

class InitChatState extends ChatState {}

class GettingDataState extends ChatState {}

class GetFailDataState extends ChatState {}

class GotDataState extends ChatState {
  GotDataState({List<Message> listMessage}) : super(listMessage: listMessage);
}
