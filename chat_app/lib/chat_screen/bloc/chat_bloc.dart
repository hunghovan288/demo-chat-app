import 'package:chatapp/chat_screen/bloc/chat_event.dart';
import 'package:chatapp/chat_screen/bloc/chat_state.dart';
import 'package:chatapp/model/message.dart';
import 'package:chatapp/model/user.dart';
import 'package:chatapp/server/mock_server.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ChatBloc extends Bloc<ChatEvent, ChatState> {
  ChatServer chatServer = ChatServer();

  @override
  ChatState get initialState => InitChatState();

  @override
  Stream<ChatState> mapEventToState(ChatEvent event) async* {
    switch (event.runtimeType) {
      case GetDataEvent:
        yield* _mapGetDataEventToState(event);
        break;
      case SendMessageEvent:
        yield* _mapSendMessageEventToState(event);
        break;
    }
  }

  Stream<ChatState> _mapSendMessageEventToState(SendMessageEvent event) async* {
    final currentListMessage = state.listMessage ?? <Message>[];
    Message message = Message(
        message: event.message,
        timeCreate: DateTime.now().millisecondsSinceEpoch,
        idSender: event.idSender)
      ..itMe = true;
    currentListMessage.add(message);
    yield GotDataState(listMessage: currentListMessage);
    chatServer.saveMessage(currentListMessage, event.idGroup);
  }

  Stream<ChatState> _mapGetDataEventToState(GetDataEvent event) async* {
    yield GettingDataState();
    final data = await chatServer.getListMessageByIdGroup(
        event.idGroupChat, event.idCurrentUser);
    if (data?.isEmpty ?? true) {
      yield GetFailDataState();
      return;
    }
    yield GotDataState(listMessage: data);
  }

  String getIdGroupChat(User user, User receiver) {
    if (user.timeCreate < receiver.timeCreate) {
      return '${user.id}${receiver.id}';
    }
    return '${receiver.id}${user.id}';
  }
}
