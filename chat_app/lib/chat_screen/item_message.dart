import 'package:chatapp/model/message.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ItemMessage extends StatefulWidget {
  final Message message;

  ItemMessage({Key key, this.message}) : super(key: key);

  @override
  _ItemMessageState createState() => _ItemMessageState();
}

class _ItemMessageState extends State<ItemMessage> {
  bool _showTime = false;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        (widget.message?.itMe ?? false) ? Spacer() : SizedBox(),
        Column(
          crossAxisAlignment: (widget.message?.itMe ?? false)
              ? CrossAxisAlignment.end
              : CrossAxisAlignment.start,
          children: <Widget>[
            InkWell(
                onTap: () {
                  setState(() {
                    _showTime = !_showTime;
                  });
                },
                child: Text(
                  widget.message?.message ?? '',
                  style: TextStyle(fontSize: 25),
                )),
            _showTime
                ? Text(_formatTime(widget.message?.timeCreate ?? 1))
                : SizedBox(),
          ],
        ),
        (widget.message?.itMe ?? false) ? SizedBox() : Spacer(),
      ],
    );
  }

  String _formatTime(int millis) {
    DateTime time = DateTime.fromMillisecondsSinceEpoch(millis);
    return DateFormat('yyyy-MM-dd – kk:mm').format(time);
  }
}
