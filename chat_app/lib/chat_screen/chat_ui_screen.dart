import 'package:chatapp/chat_screen/bloc/chat_bloc.dart';
import 'package:chatapp/chat_screen/bloc/chat_event.dart';
import 'package:chatapp/chat_screen/bloc/chat_state.dart';
import 'package:chatapp/chat_screen/item_message.dart';
import 'package:chatapp/model/user.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ChatUiScreen extends StatefulWidget {
  final User currentUser;
  final User receiver;

  ChatUiScreen({Key key, this.currentUser, this.receiver}) : super(key: key);

  @override
  _ChatUiScreenState createState() => _ChatUiScreenState();
}

class _ChatUiScreenState extends State<ChatUiScreen> {
  ChatBloc _chatBloc;
  String _idGroupChat;
  TextEditingController _controller = TextEditingController();
  User _currentUser;

  @override
  void dispose() {
    _chatBloc.close();
    super.dispose();
  }

  @override
  void initState() {
    _currentUser = widget.currentUser;
    _chatBloc = ChatBloc();
    _idGroupChat = _chatBloc.getIdGroupChat(_currentUser, widget.receiver);
    _chatBloc.add(GetDataEvent(
      idGroupChat: _idGroupChat,
      idCurrentUser: _currentUser.id,
    ));
    super.initState();
  }

  void _onItemSend() {
    if (_controller.text.isEmpty) {
      return;
    }
    _chatBloc.add(SendMessageEvent(
      message: _controller.text,
      idSender: _currentUser.id,
      idGroup: _idGroupChat,
    ));
    _controller.text = '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.receiver?.name ?? ''),
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: BlocConsumer(
              bloc: _chatBloc,
              listener: (context, state) {},
              builder: (context, state) {
                if (state is GettingDataState) {
                  return Center(child: CircularProgressIndicator());
                }
                if (state is GotDataState) {
                  final listMessage = state.listMessage;
                  return Column(
                    children: <Widget>[
                      ListView.separated(
                        itemBuilder: (context, index) => ItemMessage(
                          message: listMessage[index],
                        ),
                        separatorBuilder: (context, index) {
                          return SizedBox(
                            height: 10,
                          );
                        },
                        shrinkWrap: true,
                        padding:
                            EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                        itemCount: listMessage.length,
                      )
                    ],
                  );
                }
                return Container(
                  child: Center(
                    child: Text('empty'),
                  ),
                );
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
            child: Row(
              children: <Widget>[
                Expanded(
                    child: TextField(
                  controller: _controller,
                )),
                FloatingActionButton(
                  child: Icon(Icons.send),
                  onPressed: _onItemSend,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
