import 'package:chatapp/chat_screen/chat_ui_screen.dart';
import 'package:chatapp/model/user.dart';
import 'package:chatapp/select_receiver/select_receiver.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  User _user1;
  User _user2;
  User _user3;

  @override
  void initState() {
    _user1 = User(id: 'abc1', name: 'User1', timeCreate: 1000);
    _user2 = User(id: 'abc2', name: 'User2', timeCreate: 1002);
    _user3 = User(id: 'abc3', name: 'User3', timeCreate: 1004);
    super.initState();
  }

  void _onItemUserTap(User user) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => SelectReceiverScreen(
          currentUser: user,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 100,
          ),
          Text('Welcome, Đăng nhập với tư cách'),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                RaisedButton(
                  child: Text('User1'),
                  onPressed: () {
                    _onItemUserTap(_user1);
                  },
                ),
                RaisedButton(
                  child: Text('User2'),
                  onPressed: () {
                    _onItemUserTap(_user2);
                  },
                ),
                RaisedButton(
                  child: Text('User3'),
                  onPressed: () {
                    _onItemUserTap(_user3);
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
