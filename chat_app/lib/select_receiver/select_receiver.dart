import 'package:chatapp/chat_screen/chat_ui_screen.dart';
import 'package:chatapp/model/user.dart';
import 'package:chatapp/server/mock_server.dart';
import 'package:flutter/material.dart';

class SelectReceiverScreen extends StatefulWidget {
  final User currentUser;

  SelectReceiverScreen({Key key, this.currentUser}) : super(key: key);

  @override
  _SelectReceiverScreenState createState() => _SelectReceiverScreenState();
}

class _SelectReceiverScreenState extends State<SelectReceiverScreen> {
  List<User> _listUser;

  @override
  void initState() {
    _listUser = ChatServer.getListUser();
    _listUser.removeWhere((element) => element.id == widget.currentUser.id);
    super.initState();
  }

  void _onItemTap(User receiver) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ChatUiScreen(
          currentUser: widget.currentUser,
          receiver: receiver,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Ban muon chat voi ai'),
        centerTitle: true,
      ),
      body: Container(
        color: Colors.white,
        child: ListView.separated(
          itemBuilder: (context, index) {
            return InkWell(
              onTap: () {
                _onItemTap(_listUser[index]);
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    _listUser[index].name,
                    style: TextStyle(fontSize: 25),
                  ),
                  Divider(
                    color: Colors.grey,
                    height: 1,
                  ),
                ],
              ),
            );
          },
          separatorBuilder: (context, index) {
            return SizedBox(
              height: 20,
            );
          },
          padding: EdgeInsets.symmetric(vertical: 50, horizontal: 20),
          itemCount: _listUser.length,
        ),
      ),
    );
  }
}
