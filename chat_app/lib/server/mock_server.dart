import 'dart:convert';

import 'package:chatapp/model/message.dart';
import 'package:chatapp/model/user.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChatServer {
  static List<User> getListUser() {
    return <User>[
      User(id: 'abc1', name: 'User1', timeCreate: 1000),
      User(id: 'abc2', name: 'User2', timeCreate: 1002),
      User(id: 'abc3', name: 'User3', timeCreate: 1004)
    ];
  }

  Future<List<Message>> getListMessageByIdGroup(
      String idGroup, String idCurrentUser) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final data = prefs.getString(idGroup);
    if (data == null) {
      return null;
    }
    final output = <Message>[];
    final result = json.decode(data);
    result.forEach((v) {
      final message = Message.fromJson(v);
      if (message.idSender == idCurrentUser) {
        message.itMe = true;
      }
      output.add(message);
    });
    return output;
  }

  Future<bool> saveMessage(List<Message> messages, String idGroup) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String data = jsonEncode(messages.map((e) => e.toJson()).toList());
    final result = await prefs.setString(idGroup, data);
    return result;
  }
}
